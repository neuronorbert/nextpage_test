$(function() {
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    vertical: true,
    draggable: false,
    arrows: true,
    prevArrow: '<button type="button" class="btn slick-prev">previous process <i class="icon icon-up-arrow"></i></button>',
    nextArrow: '<button type="button" class="btn slick-next">next process <i class="icon icon-down-arrow"></i></button>',
    asNavFor: '.slider-nav',
    infinite: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          vertical: false,
          fade: true,
        }
      },
    ]
  });
  $('.slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: false,
    asNavFor: '.slider-for',
    dots: false,
    arrows: true,
    prevArrow: '<button type="button" class="btn slick-prev"><i class="icon icon-up-arrow"></i></button>',
    nextArrow: '<button type="button" class="btn slick-next"><i class="icon icon-down-arrow"></i></button>',
    focusOnSelect: true,
    vertical: true,
    verticalSviping: true,
    infinite: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          vertical: false,
          prevArrow: '<button type="button" class="btn slick-prev"><i class="icon icon-left-arrow"></i></button>',
          nextArrow: '<button type="button" class="btn slick-next"><i class="icon icon-right-arrow"></i></button>',
        }
      },
    ]
  });
  // Site Animations
  $('.is-fadeInDown').animated('fadeInDown', 'fadeOutUp');
  $('.is-fadeInUp').animated('fadeInUp', 'fadeOutDown');
  $('.is-fadeInRight').animated('fadeInRight', 'fadeOutLeft');
  $('.is-fadeInLeft').animated('fadeInLeft', 'fadeOutRight');
});